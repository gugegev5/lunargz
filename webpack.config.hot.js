var path = require('path');

const htmlWebpackPlugin = require('html-webpack-plugin')
module.exports = {
    entry: {
        lunargz: './test/index.js',
    },
    devServer: {
        contentBase: path.join(__dirname, "test"), // 服务器目录
        compress: true, // gzip 压缩
        port: 9000 // 监听端口号
    },
    output: {
        path: path.resolve(__dirname, 'libs'),
        filename: '[name].js'
    },

    module: {
        loaders: [{
            test: /\.js$/,
            loader: 'babel-loader',
            exclude: [
                path.resolve(__dirname, "node_modules"),
            ],
            query: {
                plugins: ['transform-runtime'],
                presets: ['es2015', 'stage-0'],
                compact: true
            }
        }, {
            test: /\.json$/,
            loader: 'json-loader'
        }]
    },
    resolve: {
        extensions: ['*', '.js', '.json']
    },
    plugins: [
        new htmlWebpackPlugin({
            template: './test/index.html'
        })
    ]

}