#  农历天干地支

#### 介绍
js新历转换成农历干支，节气精确到秒，用于精确的农历干支计算

#### 项目结构
webpack热更新测试
webpack打包


#### 使用方法

Lunargz
```js
 import Lunargz from './libs/lunargz.js'
 let lunargz = new Lunargz(time);//传入新历时间参数
 console.log(lunargz)
``` 

#### 测试
```js
 npm i
 npm run dev
 //浏览器打开地址http://localhost:9000/
``` 
#### 打包
```js
 npm i
 npm run build
``` 


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
