var path = require('path');

module.exports = {
    entry: {
        lunargz: './index.js',
    },
    output: {
        path: path.resolve(__dirname, 'libs'),
        filename: '[name].js',
        library: 'Lunar',
        libraryTarget: "umd",
        libraryExport: 'default',
    },

    module: {
        loaders: [{
            test: /\.js$/,
            loader: 'babel-loader',
            exclude: [
                path.resolve(__dirname, "node_modules"),
            ],
            query: {
                plugins: ['transform-runtime'],
                presets: ['es2015', 'stage-0'],
                compact: true
            }
        }, {
            test: /\.json$/,
            loader: 'json-loader'
        }]
    },
    resolve: {
        extensions: ['*', '.js', '.json']
    },

}